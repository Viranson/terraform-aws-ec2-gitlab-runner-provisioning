data "aws_vpc" "default" {
  default = true
}

module "runner_security_group" {
  source   = "./modules/aws_security_group"
  for_each = var.vpc_sg_profile
  # vpc_id        = data.aws_vpc.prod_vpc.id
  vpc_sg_name   = each.value.vpc_sg_name
  description   = each.value.description
  ingress_rules = each.value.ingress_rules
  egress_rules  = each.value.egress_rules
  vpc_sg_tags = merge(
    local.common_tags, each.value.tags
  )
}

module "runner_instance" {
  source                      = "./modules/aws_instance"
  for_each                    = var.ec2_instance_profile
  ec2_ami                     = var.ami
  user_data                   = file("./setup.sh")
  instance_type               = each.value.instance_type
  associate_public_ip_address = each.value.associate_public_ip_address
  key_name                    = each.value.key_name
  security_group_ids          = ["${module.runner_security_group[each.value.vpc_sg_name].vpc_sg_id}"]
  ec2_instance_tags = merge(
    local.common_tags, each.value.tags
  )
}