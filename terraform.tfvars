# region     = "us-east-1"
# ami = "ami-06178cf087598769c"
access_key = ""
secret_key = ""
region     = "eu-west-3"
ami        = "ami-0dfb6769e523bf035"
# instance_type    = "t3_micro"

vpc_sg_profile = {
  runner-vpc-sg = {
    vpc_sg_name = "runner_sg"
    description = "Allow HTTP & HTTPS"
    ingress_rules = {
      allow_http_from_anywhere = {
        description      = "Allow HTTP"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
      }
      allow_https_from_anywhere = {
        description      = "Allow HTTPS"
        from_port        = 443
        to_port          = 443
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
      }
    }
    egress_rules = {
      allow_all_from_anywhere = {
        description      = "Allow All"
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
      }
    }
    tags = {
      Name = "runner-sg"
    }
  }
}

ec2_instance_profile = {
  runner-ec2-host = {
    instance_type               = "t3.medium"
    key_name                    = "runner-host-key"
    vpc_sg_name                 = "runner-vpc-sg"
    associate_public_ip_address = true
    tags = {
      Name = "runner-ec2-host"
    }
  }
}