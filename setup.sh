#!/bin/bash
#INSTALL NGINX
sudo apt update
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx

# this script is only tested on ubuntu focal 20.04 (LTS)
user=ubuntu
# install docker
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update                                                                                                                                                                                                                                                            
sudo apt-get install -y docker-ce docker-ce-cli docker-compose containerd.io                                                                                                                                                                                                                  
sudo systemctl enable docker                                                                                                                                                                                                                                                   
sudo systemctl start docker                                                                                                                                                                                                                                                    
sudo usermod -aG docker $user

# Install Gitlab Runner
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

# Add Gitlab Runner User to docker Group
sudo usermod -aG docker gitlab-runner

# Start Gitlab Runner
sudo gitlab-runner start

# Register Gitlab Runner
#sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941J8XXELU8V3AtsSGWxqGz


newgrp docker